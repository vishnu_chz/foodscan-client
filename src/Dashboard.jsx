import { useState, useEffect } from "react";
import {
	Alert,
	Box,
	Button,
	Container,
	Divider,
	Typography,
} from "@mui/material";
import TableComponent from "./components/Table";

const baseUrl = import.meta.env.VITE_URL;

const Dashboard = () => {
	const [data, setData] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [total, setTotal] = useState(0);
	const [info, setInfo] = useState("");

	const fetchData = () => {
		const billID = 1;
		fetch(`${baseUrl}/invoice/${billID}`)
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
				setData(data);
			});
	};

	const computeTotal = () => {
		const billID = 1;
		fetch(`${baseUrl}/bill/${billID}/total`)
			.then((response) => response.json())
			.then((total_price) => {
				setTotal(total_price);
			});
	};

	useEffect(() => {
		fetchData();
	}, []);

	useEffect(() => {
		computeTotal();
	}, [data]);

	const delayText = () => {
		setTimeout(() => {
			setInfo("Add Weight");
		}, 1000);
	};

	const handleClickDelay = async () => {
		delayText();
		handleClick();
	};

	const handleClick = async () => {
		setIsLoading(true);
		const billID = 1;
		fetch(`${baseUrl}/invoice/${billID}`, { method: "POST" })
			.then((response) => response.json())
			.then((item) => {
				setData((prevData) => [...prevData, item]);
				setIsLoading(false);
				setInfo("");
			})
			.catch((error) => {
				console.error(error);
				setIsLoading(false);
				setInfo("");
			});
	};

	const handleDelete = async (id) => {
		fetch(`${baseUrl}/invoice/${id}`, { method: "DELETE" })
			.then((response) => response.json())
			.then((id) => {
				fetchData();
			});
	};

	return (
		<Container sx={{ my: 3 }} border-rounded>
			<Box sx={{ my: 3 }}>
				<Typography
					variant='h4'
					component='div'
					sx={{ textAlign: "center", fontWeight: "400" }}
				>
					Shopping List
				</Typography>
				<Divider sx={{ my: 2 }} />

				{data.length !== 0 ? (
					<TableComponent data={data} handleDelete={handleDelete} />
				) : (
					<Typography
						variant='subtitle1'
						component={"div"}
						sx={{ textAlign: "center" }}
					>
						No Data
					</Typography>
				)}
			</Box>
			<Box sx={{ display: "flex", justifyContent: "space-between" }}>
				<Button
					variant='contained'
					onClick={handleClickDelay}
					disabled={isLoading}
				>
					Start Scan
				</Button>
				<Button variant='contained'>Clear Bill</Button>
			</Box>
			{info.length !== 0 && (
				<Box sx={{ my: 2 }}>
					<Alert severity='info'>{info}</Alert>
				</Box>
			)}
			<Box>
				<Typography
					variant='h6'
					component={"div"}
					sx={{ textAlign: "center" }}
				>
					Total : {total}
				</Typography>
			</Box>
		</Container>
	);
};

export default Dashboard;
